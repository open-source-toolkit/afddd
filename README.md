# Win7最后支持的VS Code X86版本

本仓库提供了一个适用于Windows 7操作系统的最后支持的VS Code X86版本资源文件。这些版本是专门为仍在使用Windows 7的用户设计的，确保他们能够继续使用Visual Studio Code进行开发工作。

## 资源文件列表

- **VSCodeUserSetup-ia32-1.70.1.exe**
- **VSCodeUserSetup-ia32-1.70.2.exe**

## 使用说明

1. **下载资源文件**：
   - 点击上述资源文件链接，下载对应的安装包。

2. **安装VS Code**：
   - 运行下载的`.exe`文件，按照提示完成安装过程。

3. **启动VS Code**：
   - 安装完成后，您可以在开始菜单或桌面上找到VS Code的快捷方式，点击启动即可。

## 注意事项

- 这些版本是专门为Windows 7用户提供的，如果您使用的是其他操作系统或架构，请下载相应的版本。
- 由于Windows 7已经停止官方支持，建议用户尽快升级到更新的操作系统以获得更好的安全性和性能。

## 贡献

如果您有任何问题或建议，欢迎提交Issue或Pull Request。

## 许可证

本仓库中的资源文件遵循Visual Studio Code的原始许可证。详情请参阅[LICENSE](LICENSE)文件。